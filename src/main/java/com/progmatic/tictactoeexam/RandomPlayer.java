/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;

/**
 *
 * @author progmatic
 */
public class RandomPlayer extends AbstractPlayer {
    
    public RandomPlayer(PlayerType p) {
        super(p);
    }
    
    @Override
    public PlayerType getMyType() {
        return super.getMyType(); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Cell nextMove(Board b) {
        
        
        
        
        if (b.emptyCells().isEmpty()) {
            return null;
        } else {
            Cell cell = b.emptyCells().get(0);
            cell.setCellsPlayer(myType);
            return cell;
        }
        
    }
    
}
