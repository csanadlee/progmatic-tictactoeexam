/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author progmatic
 */
public class tttBoard implements Board {

    List<Cell> row1 = new ArrayList<>();
    List<Cell> row2 = new ArrayList<>();
    List<Cell> row3 = new ArrayList<>();

    public tttBoard() {

        for (int rowIdx = 0; rowIdx < 3; rowIdx++) {
            for (int colIdx = 0; colIdx < 3; colIdx++) {
                Cell cell = new Cell(rowIdx, colIdx, PlayerType.EMPTY);
                switch (rowIdx) {
                    case 0:
                        row1.add(cell);
                        break;
                    case 1:
                        row2.add(cell);
                        break;
                    case 2:
                        row3.add(cell);
                        break;
                    default:
                        System.out.println("valami nem döfi, a cellafeltöltésnél");
                        break;
                }
            }
        }
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {

        CellException cellexception = new CellException(rowIdx, colIdx, "valami nem döfi - a cellalekérdezésnél;- például nincs ilyen index");

        if (colIdx > 2 || colIdx < 0) {
            throw cellexception;
        }

        switch (rowIdx) {
            case 0:
                return row1.get(colIdx).getCellsPlayer();

            case 1:
                return row2.get(colIdx).getCellsPlayer();

            case 2:
                return row3.get(colIdx).getCellsPlayer();

            default:

                throw cellexception;

        }

    }

    @Override
    public void put(Cell cell) throws CellException {
        cell.getCellsPlayer();
        cell.getRow();
        cell.getCol();
        CellException cellexception = new CellException(cell.getRow(), cell.getCol(), "valami nem döfi - a cellaértének beállításánál;- például nincs ilyen index");

        switch (cell.getRow()) {
            case 0:
                if (row1.get(cell.getCol()).getCellsPlayer().equals(PlayerType.EMPTY)) {
                    row1.get(cell.getCol()).setCellsPlayer(cell.getCellsPlayer());
                }else {
                    throw cellexception;
                }
                break;
            case 1:
                if (row2.get(cell.getCol()).getCellsPlayer().equals(PlayerType.EMPTY)) {
                    row2.get(cell.getCol()).setCellsPlayer(cell.getCellsPlayer());

                } else {
                    throw cellexception;
                }
                break;
            case 2:
                if (row3.get(cell.getCol()).getCellsPlayer().equals(PlayerType.EMPTY)) {
                    row3.get(cell.getCol()).setCellsPlayer(cell.getCellsPlayer());
                }else {
                    throw cellexception;
                }
                break;
            default:
                throw cellexception;
        }

    }

    @Override
    public boolean hasWon(PlayerType p) {
        boolean boo = false;

        if (row1.get(0).getCellsPlayer().equals(p) && row1.get(1).getCellsPlayer().equals(p) && row1.get(2).getCellsPlayer().equals(p)) {
            boo = true;
        }
        if (row2.get(0).getCellsPlayer().equals(p) && row2.get(1).getCellsPlayer().equals(p) && row2.get(2).getCellsPlayer().equals(p)) {
            boo = true;
        }
        if (row3.get(0).getCellsPlayer().equals(p) && row3.get(1).getCellsPlayer().equals(p) && row3.get(2).getCellsPlayer().equals(p)) {
            boo = true;
        }

        if (row1.get(0).getCellsPlayer().equals(p) && row2.get(0).getCellsPlayer().equals(p) && row3.get(0).getCellsPlayer().equals(p)) {
            boo = true;
        }
        if (row1.get(1).getCellsPlayer().equals(p) && row2.get(1).getCellsPlayer().equals(p) && row3.get(1).getCellsPlayer().equals(p)) {
            boo = true;
        }
        if (row1.get(2).getCellsPlayer().equals(p) && row2.get(2).getCellsPlayer().equals(p) && row3.get(2).getCellsPlayer().equals(p)) {
            boo = true;
        }

        if (row1.get(0).getCellsPlayer().equals(p) && row2.get(1).getCellsPlayer().equals(p) && row3.get(2).getCellsPlayer().equals(p)) {
            boo = true;
        }
        if (row1.get(2).getCellsPlayer().equals(p) && row2.get(1).getCellsPlayer().equals(p) && row3.get(0).getCellsPlayer().equals(p)) {
            boo = true;
        }
        if (row3.get(0).getCellsPlayer().equals(p) && row2.get(1).getCellsPlayer().equals(p) && row1.get(2).getCellsPlayer().equals(p)) {
            boo = true;
        }
        if (row3.get(2).getCellsPlayer().equals(p) && row2.get(1).getCellsPlayer().equals(p) && row1.get(0).getCellsPlayer().equals(p)) {
            boo = true;
        }

        return boo;
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> emptycellz = new ArrayList<>();
        for (Cell cell : row1) {
            if (cell.getCellsPlayer().equals(PlayerType.EMPTY)) {
                emptycellz.add(cell);
            }

        }
        for (Cell cell : row2) {
            if (cell.getCellsPlayer().equals(PlayerType.EMPTY)) {
                emptycellz.add(cell);
            }

        }
        for (Cell cell : row3) {
            if (cell.getCellsPlayer().equals(PlayerType.EMPTY)) {
                emptycellz.add(cell);
            }

        }
        return emptycellz;

    }
}
